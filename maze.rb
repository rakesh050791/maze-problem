class Maze
 DIRECTIONS = [ [1, 0], [-1, 0], [0, 1], [0, -1] ]

 def initialize(width, height)
   @width   = width
   @height  = height
   @start_x = rand(width)
   @start_y = 0
   @end_x   = rand(width)
   @end_y   = height - 1

   @vertical_walls   = Array.new(width) { Array.new(height, true) }
   @horizontal_walls = Array.new(width) { Array.new(height, true) }
     @path = Array.new(width) { Array.new(height) }

   @horizontal_walls[@end_x][@end_y] = false
   generate
 end

 # Print a maze.
 def print
   puts @width.times.inject("+") {|str, x| str << (x == @start_x ? "   +" : "---+")}

   @height.times do |y|
     line = @width.times.inject("|") do |str, x|
       str << (@path[x][y] ? " * " : "   ") << (@vertical_walls[x][y] ? "|" : " ")
     end
     puts line
     puts @width.times.inject("+") {|str, x| str << (@horizontal_walls[x][y] ? "---+" : "   +")}
   end
 end

 private

 def reset_visiting_state
   @visited = Array.new(@width) { Array.new(@height) }
 end

 def move_valid?(x, y)
   (0...@width).cover?(x) && (0...@height).cover?(y) && !@visited[x][y]
 end

 def generate
   reset_visiting_state
   generate_visit_cell(@start_x, @start_y)
 end

 def generate_visit_cell(x, y)
   # Mark cell as visited.
   @visited[x][y] = true

   # Randomly get coordinates of surrounding cells (may be outside
   # of the maze range, will be sorted out later).
   coordinates = DIRECTIONS.shuffle.map { |dx, dy| [x + dx, y + dy] }

   for new_x, new_y in coordinates
     next unless move_valid?(new_x, new_y)

     # Recurse if it was possible to connect the current and
     # the cell (this recursion is the "depth-first" part).
     connect_cells(x, y, new_x, new_y)
     generate_visit_cell(new_x, new_y)
   end
 end

 # Try to connect two cells. Returns whether it was valid to do so.
 def connect_cells(x1, y1, x2, y2)
   if x1 == x2
      @horizontal_walls[x1][ [y1, y2].min ] = false
   else
      @vertical_walls[ [x1, x2].min ][y1] = false
   end
 end
end

class Maze
 def solve
   path = find_path
   if path
     # Mark the cells that make up the shortest path.
     for x, y in path
       @path[x][y] = true
     end
   else
     puts "No solution found?!"
   end
 end

 def find_path(end_x=@end_x, end_y=@end_y)
   reset_visiting_state

   @queue = []

enqueue_cell([], @start_x, @start_y)

   path = nil
   until path || @queue.empty?
     path = solve_visit_cell(end_x, end_y)
   end
   path
 end

 def path_to_each_cell_available
   path_to_each_cell = Array.new(@width) { Array.new(@height) }
   p path_to_each_cell
   @height.times do |y|
     @width.times do |x|
       unless path_to_each_cell[x][y]
         path_to_each_cell[x][y] = find_path(x,y) ? true : false
       end
     end
   end
   path_to_each_cell
 end

 private

 # Maze solving visiting method.
 def solve_visit_cell(end_x,end_y)
   # Get the next path.
   path = @queue.shift
   # The cell to visit is the last entry in the path.
   x, y = path.last

   # Have we reached the end yet?
   return path  if x == end_x && y == end_y

   # Mark cell as visited.
   @visited[x][y] = true

   for dx, dy in DIRECTIONS
     if dx.nonzero?
       # Left / Right
       new_x = x + dx
       if move_valid?(new_x, y) && !@vertical_walls[ [x, new_x].min ][y]
         enqueue_cell(path, new_x, y)
       end
     else
       # Top / Bottom
       new_y = y + dy
       if move_valid?(x, new_y) && !@horizontal_walls[x][ [y, new_y].min ]
         enqueue_cell(path, x, new_y)
       end
     end
   end
   nil         # No solution yet.
 end

 def enqueue_cell(path, x, y)
   # Add new coordinates to the current path and enqueue the new path.
   @queue << path + [[x, y]]
 end
end

maze = Maze.new 10, 5
maze.solve
maze.print